package com.orbit.qr.model;

/**
 * Created by ashiq on 11/13/2017.
 */

public class ResultModel {

    private String result;
    private int type;
    private String date;
    private long timestamp;

    public ResultModel(String result, int type, String date, long timestamp) {
        this.result = result;
        this.type = type;
        this.date = date;
        this.timestamp = timestamp;
    }


    public String getResult() {
        return result;
    }

    public int getType() {
        return type;
    }

    public String getDate() {
        return date;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
