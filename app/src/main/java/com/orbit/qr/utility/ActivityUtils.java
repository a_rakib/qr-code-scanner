package com.orbit.qr.utility;

import android.app.Activity;
import android.content.Intent;

import com.orbit.qr.activity.ResultActivity;


/**
 * Created by Ashiq on 5/11/16.
 */
public class ActivityUtils {

    private static ActivityUtils sActivityUtils = null;

    public static ActivityUtils getInstance() {
        if (sActivityUtils == null) {
            sActivityUtils = new ActivityUtils();
        }
        return sActivityUtils;
    }

    public void invokeActivity(Activity activity, Class<?> tClass, boolean shouldFinish) {
        Intent intent = new Intent(activity, tClass);
        activity.startActivity(intent);
        if (shouldFinish) {
            activity.finish();
        }
    }

    public void invokeResultActivity(Activity activity, String result) {
        Intent intent = new Intent(activity, ResultActivity.class);
        intent.putExtra("result", result);
        activity.startActivity(intent);

    }


}
