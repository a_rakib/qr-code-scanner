package com.orbit.qr.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import com.orbit.qr.R;
import com.orbit.qr.adapter.HistoryAdapter;
import com.orbit.qr.data.constant.Constants;
import com.orbit.qr.data.preference.AppPreference;
import com.orbit.qr.data.preference.PrefKey;
import com.orbit.qr.data.sqlite.ResultDbController;
import com.orbit.qr.model.ResultModel;
import com.orbit.qr.utility.AppUtils;
import com.orbit.qr.utility.DatePickerUtils;
import com.orbit.qr.utility.DialogUtils;

public class HistoryFragment extends Fragment {

    private Activity mActivity;
    private Context mContext;

    private TextView noResultView;
    private RecyclerView mRecyclerView;
    private ArrayList<ResultModel> arrayList;
    private HistoryAdapter adapter;
    private FloatingActionButton exportToEmail;//deleteAll

    private long startTime, endTime;
    private String  startTimeStr, endTimeStr;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVar();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_history, container, false);

        initView(rootView);
        initFunctionality();
        initListener();

        return rootView;
    }

    private void initVar() {
        mActivity = getActivity();
        mContext = mActivity.getApplicationContext();
    }

    private void initView(View rootView) {
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        noResultView = (TextView) rootView.findViewById(R.id.noResultView);
        //deleteAll = (FloatingActionButton) rootView.findViewById(R.id.deleteAll);
        exportToEmail = (FloatingActionButton) rootView.findViewById(R.id.exportToEmail);
    }

    private void initFunctionality() {
        arrayList = new ArrayList<>();
        adapter = new HistoryAdapter(mContext, arrayList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mRecyclerView.setAdapter(adapter);

        //arrayList.addAll(AppPreference.getInstance(mContext).getStringArray(PrefKey.RESULT_LIST));

        ResultDbController resultDbController = new ResultDbController(mContext);
        arrayList.addAll(resultDbController.getAllData());
        Collections.reverse(arrayList);
        refreshList();
    }

    private void initListener() {
        adapter.setClickListener(new HistoryAdapter.ClickListener() {
            @Override
            public void onCopyClicked(int position) {
                AppUtils.copyToClipboard(mContext, arrayList.get(position).getResult());
            }

            @Override
            public void onItemClicked(int position) {
                AppUtils.executeAction(mActivity, arrayList.get(position).getResult(), AppUtils.getResourceType(arrayList.get(position).getResult()));
            }

            @Override
            public void onItemLongClicked(final int position) {
                /*DialogUtils.showDialogPrompt(mActivity, null, getString(R.string.delete_message_item),
                        getString(R.string.yes), getString(R.string.no), true, new DialogUtils.DialogActionListener() {
                            @Override
                            public void onPositiveClick() {
                                delete(position);
                            }
                        });*/
            }

        });

        /*deleteAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showDialogPrompt(mActivity, null, getString(R.string.delete_message_all),
                        getString(R.string.yes), getString(R.string.no), true, new DialogUtils.DialogActionListener() {
                            @Override
                            public void onPositiveClick() {
                                deleteAll();
                            }
                        });
            }
        });*/

        exportToEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFirstTime();
            }
        });

    }

    private void getFirstTime() {


            AppUtils.showToast(mContext, "Select start Date");

            FragmentManager fragmentManager = getFragmentManager();
            DatePickerUtils datePickerUtils = new DatePickerUtils();
            datePickerUtils.setDatePickerListener(new DatePickerUtils.DatePickerListener() {
                @Override
                public void onDateSelected(String date, long millis) {
                    startTime = millis;
                    startTimeStr = AppUtils.getDateFromMillis(startTime);
                    getLastTime();
                }
            });
            datePickerUtils.show(fragmentManager, "datePicker1");
    }

    private void getLastTime() {
        AppUtils.showToast(mContext, "Select end Date");

        FragmentManager fragmentManager = getFragmentManager();
        DatePickerUtils datePickerUtils = new DatePickerUtils();
        datePickerUtils.setDatePickerListener(new DatePickerUtils.DatePickerListener() {
            @Override
            public void onDateSelected(String date, long millis) {
                endTime = millis;
                endTimeStr = AppUtils.getDateFromMillis(endTime);
                ResultDbController resultDbController = new ResultDbController(mContext);
                ArrayList<ResultModel> arrayList = resultDbController.getRangeData(startTime, endTime);

                if(!arrayList.isEmpty()) {

                    StringBuilder data = new StringBuilder();

                    data.append("Count: ").append(String.valueOf(arrayList.size())).append("\n");
                    for(ResultModel resultModel : arrayList) {
                        data.append(resultModel.getResult()).append(" - Till ").append(resultModel.getDate()).append("\n");
                    }

                    AppUtils.sendEmail(mActivity, Constants.EXPORT_EMAIL, "Export result of Orbit QR from "+startTimeStr+" to "+endTimeStr, data.toString());
                } else {
                    AppUtils.showToast(mContext, "No result found in between selected dates");
                }


            }
        });
        datePickerUtils.show(fragmentManager, "datePicker2");
    }

    /*private void delete(int position) {
        AppPreference.getInstance(mContext).setStringArray(PrefKey.RESULT_LIST, null);
        arrayList.remove(position);
        AppPreference.getInstance(mContext).setStringArray(PrefKey.RESULT_LIST, arrayList);
        refreshList();
    }

    private void deleteAll() {
        AppPreference.getInstance(mContext).setStringArray(PrefKey.RESULT_LIST, null);
        arrayList.clear();
        refreshList();
    }*/

    private void refreshList() {
        if(arrayList.isEmpty()) {
            noResultView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            //deleteAll.hide();
        } else {
            noResultView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
            //deleteAll.show();
        }
        adapter.notifyDataSetChanged();
    }





}
