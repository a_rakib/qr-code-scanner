package com.orbit.qr.data.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.orbit.qr.model.ResultModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ashiq on 7/26/16.
 */
public class ResultDbController {

    private SQLiteDatabase db;

    private static final String READ = "read", UNREAD = "unread";

    public ResultDbController(Context context) {
        db = DbHelper.getInstance(context).getWritableDatabase();
    }

    public int insertData(String result, int type, String date, String timestamp) {

        ContentValues values = new ContentValues();
        values.put(DbConstants.COLUMN_RESULT, result);
        values.put(DbConstants.COLUMN_TYPE, type);
        values.put(DbConstants.COLUMN_DATE, date);
        values.put(DbConstants.COLUMN_TIMESTAMP, timestamp);

        // Insert the new row, returning the primary key value of the new row
        return (int) db.insert(
                DbConstants.TABLE_NAME,
                DbConstants.COLUMN_NAME_NULLABLE,
                values);
    }

    public ArrayList<ResultModel> getAllData() {


        String[] projection = {
                DbConstants._ID,
                DbConstants.COLUMN_RESULT,
                DbConstants.COLUMN_TYPE,
                DbConstants.COLUMN_DATE,
                DbConstants.COLUMN_TIMESTAMP
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = DbConstants._ID + " DESC";

        Cursor c = db.query(
                DbConstants.TABLE_NAME,  // The table name to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return fetchData(c);
    }

    public ArrayList<ResultModel> getRangeData(long startDate, long endDate) {

        try {

            String q = "SELECT * FROM "+DbConstants.TABLE_NAME+" WHERE "+DbConstants.COLUMN_TIMESTAMP+" BETWEEN "+startDate+" AND "+endDate;
            Cursor c = db.rawQuery(q, null);

            return fetchData(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

/*    public ArrayList<ResultModel> getUnreadData() {


        String[] projection = {
                DbConstants._ID,
                DbConstants.COLUMN_RESULT,
                DbConstants.COLUMN_TYPE,
                DbConstants.COLUMN_DATE
        };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = DbConstants._ID + " DESC";
        String selection = DbConstants.COLUMN_NOT_READ_STATUS + "=?";
        String[] selectionArgs = {UNREAD};

        Cursor c = db.query(
                DbConstants.TABLE_NAME,  // The table name to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );

        return fetchData(c);
    }*/

    private ArrayList<ResultModel> fetchData(Cursor c) {
        ArrayList<ResultModel> ntyDataArray = new ArrayList<>();

        if(c != null) {
            if (c.moveToFirst()) {
                do {
                    // get  the  data into array,or class variable
                    int itemId = c.getInt(c.getColumnIndexOrThrow(DbConstants._ID));
                    String result = c.getString(c.getColumnIndexOrThrow(DbConstants.COLUMN_RESULT));
                    int type = c.getInt(c.getColumnIndexOrThrow(DbConstants.COLUMN_TYPE));
                    String data = c.getString(c.getColumnIndexOrThrow(DbConstants.COLUMN_DATE));
                    String timestamp = c.getString(c.getColumnIndexOrThrow(DbConstants.COLUMN_TIMESTAMP));
                    long longTime = Long.parseLong(timestamp);

                    // wrap up data list and return
                    ntyDataArray.add(new ResultModel(result, type, data, longTime));
                } while (c.moveToNext());
            }
            c.close();
        }
        return ntyDataArray;
    }

/*    public void updateStatus(int itemId, boolean read) {

        String readStatus = UNREAD;
        if (read) {
            readStatus = READ;
        }

        // New value for one column
        ContentValues values = new ContentValues();
        values.put(DbConstants.COLUMN_NOT_READ_STATUS, readStatus);

        // Which row to update, based on the ID
        String selection = DbConstants._ID + "=?";
        String[] selectionArgs = {String.valueOf(itemId)};

        db.update(
                DbConstants.NOTIFICATION_TABLE_NAME,
                values,
                selection,
                selectionArgs);
    }*/

}
