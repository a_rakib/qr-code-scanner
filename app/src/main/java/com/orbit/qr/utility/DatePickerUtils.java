package com.orbit.qr.utility;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;


import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Ashiq on 6/1/16.
 */
public class DatePickerUtils extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private DatePickerListener datePickerListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void setDatePickerListener(DatePickerListener datePickerListener) {
        this.datePickerListener = datePickerListener;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        if(datePickerListener != null &&  view.isShown()) {
            int correctMonth = month + 1;
            Calendar calendar = new GregorianCalendar(year, month, day);
            datePickerListener.onDateSelected(year+"-"+correctMonth+"-"+day, calendar.getTimeInMillis());
        }
    }

    public interface DatePickerListener {
        public void onDateSelected(String date, long millis);
    }

}
