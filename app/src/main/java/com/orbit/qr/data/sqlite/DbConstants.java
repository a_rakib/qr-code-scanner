package com.orbit.qr.data.sqlite;

import android.provider.BaseColumns;

/**
 * Created by Ashiq on 7/26/16.
 */
public class DbConstants implements BaseColumns {

    // commons
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    public static final String COLUMN_NAME_NULLABLE = null;

    // notification
    public static final String TABLE_NAME = "results_table";

    public static final String COLUMN_RESULT = "result";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    public static final String SQL_CREATE_NOTIFICATION_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_RESULT + TEXT_TYPE + COMMA_SEP +
                    COLUMN_TYPE + " INTEGER" + COMMA_SEP +
                    COLUMN_DATE + TEXT_TYPE + COMMA_SEP +
                    COLUMN_TIMESTAMP + TEXT_TYPE +
                    " )";


    public static final String SQL_DELETE_NOTIFICATION_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

}
